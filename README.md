# ODU Biology Scripts Repository

This repository contains documentation and scripts for useful Biological things.

# instructions for starting:
cd into the directory where you want this repository to live on your own computer
git clone https://bitbucket.org/ODUSciScripts/odu_sci_scripts.git
this will download everything in the repository to your computer

# instructions for adding:
cd into the odu_biol_scripts directory
save your file in here
git add yourfile
git commit -m 'some message about your file e.g. adding yourfile'
git push -u origin master